#pragma once

#include <map>
#include <mutex>
#include <queue>
#include <condition_variable>

using namespace std;

#include "User.h"
#include "Room.h"
#include "RecievedMessage.h"
#include "Game.h"
#include "DataBase.h"


class TriviaServer
{
public:
	TriviaServer(); // TODO the database.
	~TriviaServer(); // TODO

	void serve();

private:
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET client_sock); // TO continue.
	void safeDeleteUser(RecievedMessage* rm); // TODO

	User* handlerSignin(RecievedMessage* rm);
	bool handlerSignup(RecievedMessage* rm); 
	void handlerSignout(RecievedMessage* rm); // TODO

	void handleLeaveGame(RecievedMessage* rm); // TODO
	void handleStartGame(RecievedMessage* rm); // TODO
	void handlePlayerAnswer(RecievedMessage* rm); // TODO

	bool handleCreateRoom(RecievedMessage* rm);
	bool handleCloseRoom(RecievedMessage* rm); // TODO
	bool handleJoinRoom(RecievedMessage* rm);
	bool handleLeaveRoom(RecievedMessage* rm); // TODO
	void handleGetUsersInRoom(RecievedMessage* rm); // TODO
	void handleGetRooms(RecievedMessage* rm);

	void handleGetBestScores(RecievedMessage* rm); // TODO
	void handleGetPasonalStatus(RecievedMessage* rm); // TODO

	void handleRecievedMessage();
	void addRecievedMessage(RecievedMessage* rm);
	RecievedMessage* bulidRecievedMessage(SOCKET client_sock, int msgCode);

	//getters
	User* getUserByName(std::string username);
	User* getUserBySocket(SOCKET sock);
	Room* getRoomById(int id);

	// parameters
	SOCKET _socket;
	std::map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	std::map<int, Room*> _roomsList;
	std::mutex _mtxRecievedMessage;
	std::queue<RecievedMessage*> _queRcvMessage;
	
	static int _rooMIdSequence;


	//std::map<string, string> _usersSginUp; // it help insted of DataBase
};