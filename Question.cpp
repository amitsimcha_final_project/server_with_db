#include "Question.h"

Question::Question(int id, std::string question, std::string correctAnswer, std::string answer2, std::string answer3, std::string answer4)
{
	_id = id;
	_question = question;

	std::string ans[4] = {answer2, answer3, answer4, correctAnswer};
	int i = 0;
	int randIndex = 0;
	std::string temp;
	int isFull = 0;

	while (true)
	{
		temp = ans[i];
		randIndex = (rand() % 4);
		_correctAnswerIndex = randIndex;
		isFull = 0;

		// checks if it has nothing in it ("")
		if (_answers[randIndex] == "")
		{
			_answers[randIndex] = temp;
			i++;
		}

		//checks if all the celles are full
		if ((_answers[0] != "") && (_answers[1] != "") && (_answers[2] != "") && (_answers[3] != ""))
		{
			break;
		}
	}
}


std::string Question::getQuestion()
{
	return _question;
}


std::string * Question::getAnswers()
{
	return _answers;
}


int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}


int Question::getId()
{
	return _id;
}