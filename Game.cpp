#include "Game.h"
#include <algorithm>

Game::Game(const std::vector<User*>& players, int questionsNo, DataBase& db) : _db(db), _players(players), _questions_no(questionsNo), _currQuestionIndex(1) // or 0.
{
	bool isInsert = false;

	// try to insert the game to the db.
	try
	{
		isInsert = insertGameToDB();
	}
	catch (std::string e)
	{
		std::cout << "ERROR: " << e << std::endl;
	}


	// init the questions.
	if (isInsert)
		initQuestionsFromDB();


	// init the score list of the players.
	std::vector<User*>::iterator it;
	for (it = _players.begin(); it != _players.end(); ++it)
	{
		std::string name = (*it)->getUsername();
		_results.insert(std::pair<std::string, int>(name, 0));

		// set the currGame to all the players.
		(*it)->setGame(this);
	}
}


Game::~Game() 
{
	_questions.clear();
	_players.clear();
}


void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}


void Game::handleFinishGame()
{
	std::string codeToClient = "121";
	std::string messageToClient;
	
	_db.updateGameStatus(getID());
	
	// bulid the message.
	messageToClient += codeToClient + std::to_string(_results.size());
	std::map<std::string, int>::iterator itScores;
	for (itScores = _results.begin(); itScores != _results.end(); ++itScores)
	{
		messageToClient += Helper::getPaddedNumber(itScores->first.length(), 2) + itScores->first + Helper::getPaddedNumber(itScores->second, 2);
	}

	//send the message
	std::vector<User*>::iterator itUsers;
	for (itUsers = _players.begin(); itUsers != _players.end(); ++itUsers)
	{
		try
		{
			(*itUsers)->send(messageToClient);
			(*itUsers)->clearGame(); // clear the game from user.
		
			(*itUsers)->clearRoom(); // clear the room from the user.
		}
		catch (const std::exception e) {}
	}
}


bool Game::handleNextTurn()
{
	bool isActive = true;

	// check if dont have players in the game.
	if (_players.size() == 0)
	{
		handleFinishGame();
		isActive = false;
	}
	else
	{
		// check if all the users answer on the question.
		if (_currentTurnAnswers == _players.size())
		{
			//// check if the last question question was the last.
			if (_currQuestionIndex == _questions_no)
			{
				handleFinishGame();
				isActive = false;
			}
			else
			{
				_currQuestionIndex++;
				_questions.erase(_questions.begin()); // earse the last question from the top.
				sendQuestionToAllUsers();
			}
		}	
	}
	return isActive;
}


bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	bool isActiveGame;

	std::string username = user->getUsername();

	_currentTurnAnswers++;
	std::string codeToClient = "120";
	int questionId;;
	int	correctAnswerID;
	std::string userAnswerStr;
	bool isCorrect = false;
	std::string messageToClient;
	
	
	questionId = _questions[0]->getId(); //get the id of question.
	correctAnswerID = _questions[0]->getCorrectAnswerIndex(); // get the ID of the correct answer.
		

	// if the user dont answer on the time.
	if (answerNo == 5) 
		userAnswerStr = "";
	else
		userAnswerStr = _questions[0]->getAnswers()[answerNo - 1]; // get the string of the user answer.
	
	
	// check if the user answer is correct.
	if (answerNo - 1 == correctAnswerID) // -1 because the indexes start from 0.
	{
		isCorrect = true;

		std::map<std::string, int>::iterator it;
		for (it = _results.begin(); it != _results.end(); ++it)
		{
			if (it->first == user->getUsername())
			{
				it->second++;
				break;
			}
		}
		messageToClient = codeToClient + "1";
	}
	else
	{
		messageToClient = codeToClient + "0";
	}

	_db.addAnswerToPlayer(getID(), username, questionId, userAnswerStr, isCorrect, time);   // if the answer is 5, the text will be "".
		
	user->send(messageToClient); // send the message.
	isActiveGame = handleNextTurn();
	
	return isActiveGame;
}


BOOL Game::insertGameToDB()
{
	bool isInsert = true;
	int gameId = _db.insertNewGame();
	
	if (gameId == 0)
	{
		throw std::string("ERROR: the game is not inserted to DB");
		isInsert = false;
	}

	_id = gameId;

	return isInsert;
}


void Game::initQuestionsFromDB()
{
	_questions = _db.initQuestions(_questions_no);
}


void Game::sendQuestionToAllUsers()
{
	std::string codeToClient = "118";
	std::string messageToClient;

	// get the question object.
	Question *q = _questions.front();
	
	// get array of answers.
	std::string *answers = q->getAnswers();
	
	// get the question and the answers.
	std::string ans1 = answers[0];
	std::string ans2 = answers[1];
	std::string ans3 = answers[2];
	std::string ans4 = answers[3];
	std::string question = q->getQuestion();

	// bulid the message.
	messageToClient = codeToClient + Helper::getPaddedNumber(question.length(), 3) + question
		+ Helper::getPaddedNumber(ans1.length(), 3) + ans1
		+ Helper::getPaddedNumber(ans2.length(), 3) + ans2
		+ Helper::getPaddedNumber(ans3.length(), 3) + ans3
		+ Helper::getPaddedNumber(ans4.length(), 3) + ans4;


	_currentTurnAnswers = 0;

	// send the message to the users.
	std::vector<User*>::iterator it;
	for (it = _players.begin(); it != _players.end(); ++it)
	{
		try
		{
			(*it)->send(messageToClient);
		}
		catch (const std::exception e)
		{
			std::cout << "ERROR: " << e.what() << std::endl;
		}
	}
}


bool Game::leaveGame(User* currUser)
{
	bool gameFinished = false, isExist = false;

	// check if the game is active.
	if (_players.size() == 1)
	{
		gameFinished = true; // becoese after the player will leave, the room is going to be without players, so the game will finish.
	}

	// find the user in the players.
	std::vector<User*>::iterator it;
	for(it = _players.begin(); it != _players.end(); ++it)
	{
		if ((*it)->getUsername() == currUser->getUsername())
		{
			isExist = true;
			(*it)->clearRoom();// clear the room from the user that leaved the game.
			break;
		}
	}

	// is the user is exist, he erase from players.
	if (isExist)
	{
		_players.erase(std::remove(_players.begin(), _players.end(), currUser), _players.end());
		handleNextTurn();
	}

	return gameFinished;
}


int Game::getID()
{
	return _id;
}
