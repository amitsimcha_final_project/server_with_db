#pragma once

#include <iostream>
#include <string>
#include <WinSock2.h>
#include <Windows.h>

#include "Helper.h"

#include "Room.h"
#include "Game.h"
class Room;
class Game;


class User
{
public:
	User(std::string username, SOCKET sock);
	void send(std::string message);

	// geters
	std::string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();

	// serters
	void setGame(Game* game);


	void clearRoom();
	bool createRoom(int roomId, std::string roomName, int maxUsers, int questionNo, int questionTime);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	void clearGame();
	bool leaveGame();

private:
	std::string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;

};