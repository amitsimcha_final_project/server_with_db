#pragma once

#include <iostream>
#include <string>

using namespace std;

class Validator
{
public:
	static bool isUsernameValid(string Username);
	static bool isPasswordValid(string password);
};