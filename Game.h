#pragma once

#include <vector>
#include <iostream>
#include <map>
#include <string>

#include "Helper.h"
#include "Question.h"
#include "DataBase.h"

#include "User.h"
class User;


class Game
{
public:
	Game(const std::vector<User*>& players, int questionsNo, DataBase& db);
	~Game(); //TODO

	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User*);
	int getID();

private:
	BOOL insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionToAllUsers();

	//parametters
	std::vector<Question*> _questions;
	std::vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	std::map<std::string, int> _results;
	int _currentTurnAnswers;

	int _id;
};