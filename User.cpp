#include "User.h"

User::User(std::string username, SOCKET sock)
{
	_username = username;
	_sock = sock;
	_currRoom = nullptr;
	_currGame = nullptr;
}


void User::send(std::string message)
{
	try
	{
		Helper::sendData(_sock, message);
	}
	catch (const std::exception e) {}
}


std::string User::getUsername()
{
	return _username;
}


SOCKET User::getSocket()
{
	return _sock;
}


Room* User::getRoom()
{
	return _currRoom;
}


Game* User::getGame() 
{
	return _currGame;
}


void User::setGame(Game* game)
{
	_currGame = game;
}


void User::clearRoom()
{
	_currRoom = nullptr;
}


bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionNo, int questionTime)
{
	if (_currRoom != nullptr) // already joined to a room
	{
		send("1141");
		return false;
	}
	else // not joined to a room
	{
		Room* newRoom = new Room(roomId, this, roomName, maxUsers, questionNo, questionTime);
		_currRoom = newRoom;
		send("1140");
		return true;
	}
}


bool User::joinRoom(Room* newRoom)
{
	if (_currRoom == nullptr) // not joined to a room
	{
		if (newRoom->joinRoom(this))
		{
			_currRoom = newRoom;
			return true;
		}
	}
	// already joined to a room
	return false;
}


void User::leaveRoom()
{
	if (_currRoom != nullptr) // already joined to a room
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}


int User::closeRoom()
{
	if (_currRoom == nullptr) // not joined to a room
	{
		return -1;
	}
	else
	{
		int isClose = _currRoom->closeRoom(this);
		if(isClose == -1)
		{
			return -1;
		}
		delete(_currRoom);
		_currRoom = nullptr;
		return isClose;
	}
}


void User::clearGame()
{
	_currGame = nullptr;
}


bool User::leaveGame()
{
	bool isActiveGame;
	if (_currGame != nullptr) // joined to a game
	{
		isActiveGame = _currGame->leaveGame(this); // get the answer if the game was finished.
		_currGame = nullptr;
	}
	else
	{
		isActiveGame = false;
	}
	return isActiveGame;
}