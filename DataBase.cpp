﻿#include "DataBase.h"

// globle veriables
int rc;
char *zErrMsg = 0;
std::string data; // each time callbackData is used the variable data will contain a diffrant value 
std::string query; // a query, to ask the data base for data 
//std::vector<std::string[6 /*the number of strings in a question*/]> questionsFromCallBack; // a vector that contains string arrays and each array is a question
//std::vector<Question*>* questionsFromCallBack = (std::vector<Question*>*)notUsed; // a vector of questions
std::vector<std::string> questionsFromCallBack; // a vector of strings, each 6 strings is a question
const std::string quoteSign = "\""; // to deal with the issue that sqlite can't get a string witout quoting signs ("") 

// אם תומר לא רוצה הצהרת משתנים גלובלים 
// אז או לשים אותם בתור מאפיינים של המחלקה 
// או להצהיר עליהם בכל פונקציה מחדש

// call back function to get a spacific data from the data base
int callbackData(void* notUsed, int argc, char** argv, char** azCol)
{
	data = argv[0];
	return 0;
}

DataBase::DataBase()
{
	// connection to the database
	rc = sqlite3_open("trivia_DB.db", &_db);

	// if dosnt work need to throw an excption
	if (rc != SQLITE_OK)
		throw std::exception(sqlite3_errmsg(_db));
}

DataBase::~DataBase()
{
	// close the db
	sqlite3_close(_db);
}

bool DataBase::isUserExists(std::string username)
{
	bool isExist = false;
	// convert the string to a string with quotes
	std::string quoted_username = quoteSign + username + quoteSign;

	query = "select username from t_users where username = " + quoted_username + ";";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);

	if (data == username) // if returns an error so that means thet the user doesn't exist
	{
		// the user dose excist
		isExist = true;
	}
	return isExist;
}

bool DataBase::addNewUser(std::string username, std::string password, std::string email)
{
	bool isAdd = false;
	// convert the string to a string with quotes
	std::string quoted_username = quoteSign + username + quoteSign;
	std::string quoted_password = quoteSign + password + quoteSign;
	std::string quoted_email = quoteSign + email + quoteSign;

	query = "insert into t_users(username, password, email) values(" + quoted_username + "," + quoted_password + "," + quoted_email + ");";
	
	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc != SQLITE_OK) // ther was a fail in the adding of the user
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	else // the user have been added
	{
		isAdd = true;
	}
	return isAdd;
}

bool DataBase::isUserAndPassMatch(std::string username, std::string password)
{
	bool isUserAndPassMatch = true;
	std::string dataFromTable[2];

	// convert the string to a string with quotes
	std::string quoted_username = quoteSign + username + quoteSign;
	std::string quoted_password = quoteSign + password + quoteSign;

	/*query = "select username from t_users where username = " + quoted_username + ";";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	
	// if the username is not found
	if (data == username)
		isUserAndPassMatch = true;
	else // means that he not found a metching username
	{
		isUserAndPassMatch = false;
		dataFromTable[0] = data; // should get the username
	}
	*/

	query = "select password from t_users where username = " + quoted_username + ";";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (data == password)
	{
		isUserAndPassMatch = true;
	}
	else // means that he not found a metching password
	{
		/*dataFromTable[1] = data; // should get the password

		if ((dataFromTable[0] == username) && (dataFromTable[1] == password))
		{
			isUserAndPassMatch = true;
		}
		else
		{*/
			isUserAndPassMatch = false;
		//}
	}

	data.clear();

	return isUserAndPassMatch;
}

std::vector<Question*> DataBase::initQuestions(int questionsNo)
{
	std::string tempStringQuestion[6] = { " ", " ", " ", " ", " ", " " };
	std::vector<Question*> randomVecOfQuestions;

	query = "select * from t_questions;";

	rc = sqlite3_exec(_db, query.c_str(), callbackQuestions, 0, &zErrMsg);
	

	int i, j;

	for (i = 0,j = 0; j < questionsNo; i+=6, j++)
	{
		tempStringQuestion[0] = questionsFromCallBack[i];     // question_id
		tempStringQuestion[1] = questionsFromCallBack[i + 1]; // question
		tempStringQuestion[2] = questionsFromCallBack[i + 2]; // correct_ans
		tempStringQuestion[3] = questionsFromCallBack[i + 3]; // ans2
		tempStringQuestion[4] = questionsFromCallBack[i + 4]; // ans3
		tempStringQuestion[5] = questionsFromCallBack[i + 5]; // ans4

		// create a new question
		Question* tempQuestion = new Question
		(
			std::stoi(tempStringQuestion[0]), 
			tempStringQuestion[1], 
			tempStringQuestion[2], 
			tempStringQuestion[3], 
			tempStringQuestion[4], 
			tempStringQuestion[5]
		);

		// add the new question to the vector that will be returnd
		randomVecOfQuestions.push_back(tempQuestion);
	}

	//randomVecOfQuestions = *(questionsFromCallBack);

	// shuffle the vector to make it randomaized
	std::random_shuffle(randomVecOfQuestions.begin(), randomVecOfQuestions.end());

	return randomVecOfQuestions;
}
  
int DataBase::insertNewGame()
{
	int roomId = 0;

	// convert the string to a string with quotes
	std::string quoted_NOW = quoteSign + "NOW" + quoteSign;
	std::string quoted_NULL = quoteSign + "NULL" + quoteSign;

	query = "insert into t_games(status, start_time, end_time) values(0," + quoted_NOW + "," + quoted_NULL + ");";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		 // the insertion was susuccfull
		roomId = (int)sqlite3_last_insert_rowid(_db);
	}

	data.clear();

	return roomId;
}

bool DataBase::updateGameStatus(int gameId)
{
	bool isUpdate = false;
	// convert the gameId(int) to a gameId(string)
	std::string str_gameId;
	std::ostringstream convert;
	convert << gameId;
	str_gameId = convert.str();

	// convert the string to a string with quotes
	std::string quoted_NOW = quoteSign + "NOW" + quoteSign;
		
	query = "update t_games set status = 1, end_time = " + quoted_NOW + " where game_id = " + str_gameId + ";";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		isUpdate = true;
	}

	data.clear();

	return isUpdate;
}

bool DataBase::addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime)
{
	bool isAdd = false;
	// convert gameId(int) to gameId(string)
	std::string str_gameId;
	std::ostringstream convert;
	convert << gameId;
	str_gameId = convert.str();

	convert.str("");
	convert.clear(); // Clear state flags.

	// convert questionId(int) to questionId(string)
	std::string str_questionId;
	convert << questionId;
	str_questionId = convert.str();

	convert.str("");
	convert.clear(); // Clear state flags.

	// convert isCorrect(bool(0 = false 1 = true)) to isCorrect(string)
	std::string str_isCorrect;
	convert << isCorrect;
	str_isCorrect = convert.str();

	convert.str("");
	convert.clear(); // Clear state flags.

	// convert answerTime(int) to answerTime(string)
	std::string str_answerTime;
	convert << answerTime;
	str_answerTime = convert.str();

	convert.str("");
	convert.clear(); // Clear state flags.

	// convert the string to a string with quotes
	std::string quoted_username = quoteSign + username + quoteSign;
	std::string quoted_answer = quoteSign + answer + quoteSign;
 
	std::string values = str_gameId + "," + quoted_username + "," + str_questionId + "," + quoted_answer + "," + str_isCorrect + "," + str_answerTime;

	// adds the answer to the player
	query = "insert into t_players_answers(game_id, username, question_id, player_answer, is_correct, answer_time) values(" + values + ");";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		// if ther are no errors so its good to go
		isAdd =  true;
	}

	data.clear();

	return isAdd;
}

std::vector<std::string> DataBase::getBestScores()
{
	return std::vector<std::string>();
}

std::vector<std::string> DataBase::getPersonalStatus(std::string username)
{
	/*std::vector<std::string> personalStatusData;
	
	// adds the username
	query = ("select username from t_users where username = %s;", username);

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	personalStatusData.push_back(data);


	// adds the password
	query = ("select password from t_users where username = %s;", username);

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	personalStatusData.push_back(data);


	// adds the email
	query = ("select email from t_users where username = %s;", username);

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	personalStatusData.push_back(data);

	return personalStatusData;*/

	return std::vector<std::string>();
}

/*int DataBase::callbackCount(void *, int, char **, char **)
{
	return 0;
}*/

int DataBase::callbackQuestions(void * notUsed,  int argc, char ** argv, char ** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		questionsFromCallBack.push_back(argv[i]);
	}

	//questionsFromCallBack = (std::vector<Question*>*)notUsed;
	/*Question* currQuestion;

	currQuestion = new Question(std::atoi(argv[0]), 
								argv[1], 
								argv[2], 
								argv[3], 
								argv[4], 
								argv[5]);


	// add the new question to the vector that will be returnd
	questionsFromCallBack->push_back(currQuestion);*/

	return 0;
}

/*int DataBase::callbackScores(void *, int, char **, char **)
{
	return 0;
}

int DataBase::callbackStatus(void *, int, char **, char **)
{
	return 0;
}*/
