#include "TriviaServer.h"
#include "helper.h"
#include "Validator.h"

#include <exception>
#include <iostream>
#include <string>

#include <WinSock2.h>
#include <Windows.h>

#define SIGN_IN 200
#define SIGN_OUT 201
#define SIGN_UP 203
#define GET_EXIST_ROOMS_LIST 205
#define GET_USERS_OF_ROOM 207
#define JOIN_ROOM 209
#define LEAVE_ROOM 211
#define CREATE_ROOM 213
#define CLOSE_ROOM 215
#define START_GAME 217
#define ANSWER_OF_QUESTION 219
#define LEAVE_GAME 222
#define GET_BEST_SCORE 223
#define GET_STATUS 225
#define EXIT_APP 299 

condition_variable cv;

// using static const instead of macros 
static const unsigned short PORT = 8826;
static const unsigned int IFACE = 0;

int TriviaServer::_rooMIdSequence = 0;

TriviaServer::TriviaServer()
{
	try
	{
		DataBase(_db);
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}

	// create a new socket.
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

}
TriviaServer::~TriviaServer()
{
	_roomsList.clear();
	_connectedUsers.clear();
	
	TRACE(__FUNCTION__ " closing accepting socket");
	try
	{
		::closesocket(_socket);
	}
	catch (...) {}

}


void TriviaServer::serve()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&TriviaServer::handleRecievedMessage, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		accept();
	}
}


void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening...");
}


void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");

	// create new thread for client	and detach from it
	std::thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();
}


void TriviaServer::clientHandler(SOCKET client_sock)
{
	RecievedMessage* currRcvMsg = nullptr;
	try
	{
		// get the first message code
		int	msgCode = Helper::getMessageTypeCode(client_sock);

		while (msgCode != EXIT_APP && msgCode != 0)
		{
			currRcvMsg = bulidRecievedMessage(client_sock, msgCode);
			addRecievedMessage(currRcvMsg);
			msgCode = Helper::getMessageTypeCode(client_sock);
		}

		currRcvMsg = bulidRecievedMessage(client_sock, EXIT_APP);
		addRecievedMessage(currRcvMsg);

	}
	catch (const std::exception& e)
	{
		currRcvMsg = bulidRecievedMessage(client_sock, EXIT_APP);
		addRecievedMessage(currRcvMsg);
	}
}

void TriviaServer::safeDeleteUser(RecievedMessage * rm)
{
	try
	{
		SOCKET clientSock = rm->getSock();
		handlerSignout(rm);
		::closesocket(clientSock);
	}
	catch (...) {}
}


bool TriviaServer::handlerSignup(RecievedMessage * rm)
{
	string codeToClient = "104";
	string username = rm->getValues()[0];
	string password = rm->getValues()[1];
	string email = rm->getValues()[2];

	if (Validator::isPasswordValid(password) == false)
	{
		Helper::sendData(rm->getSock(), codeToClient + "1");
		return false;
	}
	else if (Validator::isUsernameValid(username) == false)
	{
		Helper::sendData(rm->getSock(), codeToClient + "3");
		return false;
	}
	else
	{
		bool isExist = _db.isUserExists(username);
		if (isExist) // if the user is exist, send error.
		{
			Helper::sendData(rm->getSock(), codeToClient + "2"); // Username is already exists.
			return false;
		}
		else // if he not exist.
		{
			bool isAdd = _db.addNewUser(username, password, email);
			if (isAdd) // if the user add now
			{
				Helper::sendData(rm->getSock(), codeToClient + "0"); // succes.
			}
			else
			{
				// TODO: send error message...
			}
		}
	}
	return true;
}

void TriviaServer::handlerSignout(RecievedMessage * rm)
{
	// check if message have an user
	if (rm->getUser() != nullptr)
	{
		_connectedUsers.erase(rm->getSock());
		handleCloseRoom(rm);
		handleLeaveRoom(rm);
		handleLeaveGame(rm);
	}
}


void TriviaServer::handleRecievedMessage()
{
	while (true)
	{
		unique_lock<mutex> lck(_mtxRecievedMessage);
		// Wait for clients to enter the queue.
		if (_queRcvMessage.empty())
			cv.wait(lck);

		//take the message.
		RecievedMessage* rm = _queRcvMessage.front();
		_queRcvMessage.pop();
		lck.unlock();

		//update the user in object message
		User* user = getUserBySocket(rm->getSock());
		rm->setUser(user);

		// take the code of message.
		int msgCode = rm->getMessageCode();

		try
		{
			// chack to do something according to the code.
			switch (msgCode)
			{
			case SIGN_IN:
				handlerSignin(rm);
				break;
			case SIGN_OUT:
				handlerSignout(rm);
				break;
			case SIGN_UP:
				handlerSignup(rm); // return a bool
				break;
			case GET_EXIST_ROOMS_LIST:
				handleGetRooms(rm);
				break;
			case GET_USERS_OF_ROOM:
				handleGetUsersInRoom(rm);
				break;
			case JOIN_ROOM:
				handleJoinRoom(rm); // return bool
				break;
			case LEAVE_ROOM:
				handleLeaveRoom(rm); // return bool
				break;
			case CREATE_ROOM:
				handleCreateRoom(rm); // return bool
				break;
			case CLOSE_ROOM:
				handleCloseRoom(rm); // return bool
				break;
			case START_GAME:
				handleStartGame(rm);
				break;
			case ANSWER_OF_QUESTION:
				handlePlayerAnswer(rm);
				break;
			case LEAVE_GAME:
				handleLeaveGame(rm);
				break;
			case GET_BEST_SCORE:
				//handleGetBestScores(rm);
				break;
			case GET_STATUS:
				//handleGetPasonalStatus(rm);
				break;
			case EXIT_APP:
				safeDeleteUser(rm);
				printf("baoooooooo");
				break;

			default:
				safeDeleteUser(rm);
				break;
			}
		}
		catch (...)
		{
			safeDeleteUser(rm);
		}
	}
}


void TriviaServer::addRecievedMessage(RecievedMessage * rm)
{
	lock_guard<mutex> lck(_mtxRecievedMessage);
	_queRcvMessage.push(rm); // push new message to the dueque.

	// free the handler recieved message to work with the messages.
	cv.notify_all();
}


RecievedMessage * TriviaServer::bulidRecievedMessage(SOCKET client_sock, int msgCode)
{
	RecievedMessage* rm = nullptr;
	vector<string> values;

	// when the message code doesnt have valus.
	if (msgCode == SIGN_OUT || msgCode == GET_EXIST_ROOMS_LIST || msgCode == LEAVE_ROOM ||
		msgCode == CLOSE_ROOM || msgCode == START_GAME || msgCode == LEAVE_GAME ||
		msgCode == GET_BEST_SCORE || msgCode == GET_STATUS || msgCode == EXIT_APP)
	{
		rm = new RecievedMessage(client_sock, msgCode);
	}

	else if (msgCode == SIGN_IN)
	{
		int lenNextString = Helper::getIntPartFromSocket(client_sock, 2);
		string username = Helper::getStringPartFromSocket(client_sock, lenNextString);
		values.push_back(username);

		lenNextString = Helper::getIntPartFromSocket(client_sock, 2);
		string password = Helper::getStringPartFromSocket(client_sock, lenNextString);
		values.push_back(password);

		rm = new RecievedMessage(client_sock, msgCode, values);
		values.clear();
	}

	else if (msgCode == SIGN_UP)
	{
		int lenNextString = Helper::getIntPartFromSocket(client_sock, 2);
		string username = Helper::getStringPartFromSocket(client_sock, lenNextString);
		values.push_back(username);

		lenNextString = Helper::getIntPartFromSocket(client_sock, 2);
		string password = Helper::getStringPartFromSocket(client_sock, lenNextString);
		values.push_back(password);

		lenNextString = Helper::getIntPartFromSocket(client_sock, 2);
		string email = Helper::getStringPartFromSocket(client_sock, lenNextString);
		values.push_back(email);

		rm = new RecievedMessage(client_sock, msgCode, values);
		values.clear();
	}

	else if (msgCode == GET_USERS_OF_ROOM)
	{
		string roomID = Helper::getStringPartFromSocket(client_sock, 4);
		values.push_back(roomID);

		rm = new RecievedMessage(client_sock, msgCode, values);
		values.clear();
	}

	else if (msgCode == JOIN_ROOM)
	{
		string roomID = Helper::getStringPartFromSocket(client_sock, 4);
		values.push_back(roomID);

		rm = new RecievedMessage(client_sock, msgCode, values);
		values.clear();
	}

	else if (msgCode == CREATE_ROOM)
	{
		int lenNextString = Helper::getIntPartFromSocket(client_sock, 2);
		string roomName = Helper::getStringPartFromSocket(client_sock, lenNextString);
		values.push_back(roomName);

		string numPlayers = Helper::getStringPartFromSocket(client_sock, 1);
		values.push_back(numPlayers);

		string numQuestions = Helper::getStringPartFromSocket(client_sock, 2);
		values.push_back(numQuestions);

		string timeInSec = Helper::getStringPartFromSocket(client_sock, 2);
		values.push_back(timeInSec);

		rm = new RecievedMessage(client_sock, msgCode, values);
		values.clear();
	}

	else if (msgCode == ANSWER_OF_QUESTION)
	{
		string answerNumber = Helper::getStringPartFromSocket(client_sock, 1);
		values.push_back(answerNumber);

		string timeInSec = Helper::getStringPartFromSocket(client_sock, 2);
		values.push_back(timeInSec);

		rm = new RecievedMessage(client_sock, msgCode, values);
		values.clear();
	}

	return rm;
}


User * TriviaServer::getUserByName(std::string username)
{
	std::map<SOCKET, User*>::iterator it;

	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
	{
		if (it->second->getUsername() == username)
			return it->second;
	}
	return nullptr;
}


User * TriviaServer::getUserBySocket(SOCKET sock)
{
	std::map<SOCKET, User*>::iterator it;
	it = _connectedUsers.find(sock);

	if (it != _connectedUsers.end()) // if the user is found
	{
		return _connectedUsers.find(sock)->second;
	}

	return nullptr; // if the user isnt found
}


Room * TriviaServer::getRoomById(int id)
{
	std::map<int, Room*>::iterator it;
	it = _roomsList.find(id);

	if (it != _roomsList.end()) // if the room is found
	{
		return _roomsList.find(id)->second;
	}

	return nullptr; // if the room isnt found
}


User* TriviaServer::handlerSignin(RecievedMessage* rm)
{
	string codeToClient = "102";
	string username = rm->getValues()[0];
	string password = rm->getValues()[1];
	bool usernameAndPasswordVaild = false;


	usernameAndPasswordVaild = _db.isUserAndPassMatch(username, password);
	if (usernameAndPasswordVaild) // success in conecction
	{
		if (getUserByName(username) != nullptr) // if returns a user means that the user is already conncted
		{
			// send fail message to client (1022) user allredy conected
			Helper::sendData(rm->getSock(), codeToClient + "2");
			return nullptr;
		}
		else
		{
			// return the user that was created
			User* newSginedUpUser = new User(username, rm->getSock());

			// add the user to the connected users list
			_connectedUsers.insert(std::pair<SOCKET, User*>(rm->getSock(), newSginedUpUser));

			// send success message to client (1020)
			Helper::sendData(rm->getSock(), codeToClient + "0");

			return newSginedUpUser;
		}
	}
	else // fail in conecction
	{
		// send fail message to client (1021) wrong details
		Helper::sendData(rm->getSock(), codeToClient + "1");
		return nullptr;
	}
}


void TriviaServer::handleLeaveGame(RecievedMessage * rm)
{
	Game* currGame = rm->getUser()->getGame(); // get the game.

	// check if the game is finished (without players any more...)
	if (rm->getUser()->leaveGame() == true)
	{
		delete(currGame); // if the game was finished, the memory of the game is free.
	}
}

void TriviaServer::handleStartGame(RecievedMessage * rm)
{
	std::string codeToClient = "118";

	// create the game
	Game* newGame = nullptr;
	try
	{
		 newGame = new Game(rm->getUser()->getRoom()->getUsers(), rm->getUser()->getRoom()->getQuestionsNo(), _db);
	}
	catch (...)
	{
		rm->getUser()->send(codeToClient + "0");
	}

	// if the game is ok, erase ansd free the room from the rooms list.
	Room* room = rm->getUser()->getRoom();
	_roomsList.erase(room->getId());
	_rooMIdSequence--;
	delete(room);
	
	// start the first question.
	newGame->sendFirstQuestion();
}


void TriviaServer::handlePlayerAnswer(RecievedMessage * rm)
{
	// get the game.
	Game* currGame = rm->getUser()->getGame();

	if (currGame != nullptr)
	{
		int answerNumber = std::stoi(rm->getValues()[0]);
		int timeInSecond = std::stoi(rm->getValues()[1]);

		// just if the game was finished, free the memory.
		if (currGame->handleAnswerFromUser(rm->getUser(), answerNumber, timeInSecond) == false)
		{
			delete(currGame);
		}
	}
}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	bool returnVal = false;
	if (msg->getUser() == nullptr) // the msg don't have a user
	{
		return returnVal;
	}
	else
	{
		string roomName = msg->getValues()[0];
		string playersNumber = msg->getValues()[1];
		string questionsNumber = msg->getValues()[2];
		string questionTimeInSec = msg->getValues()[3];

		_rooMIdSequence++;

		if (msg->getUser()->createRoom(_rooMIdSequence, roomName, stoi(playersNumber), stoi(questionsNumber), stoi(questionTimeInSec)))
		{
			_roomsList.insert(std::pair<int, Room*>(_rooMIdSequence, msg->getUser()->getRoom()));
			returnVal = true;
		}
	}
	return returnVal;
}


bool TriviaServer::handleJoinRoom(RecievedMessage * rm)
{
	bool returnVal = false;

	if (rm->getUser() == nullptr)
	{
		return returnVal;
	}
	else
	{
		string roomId = rm->getValues()[0];

		if (getRoomById(stoi(roomId)) != nullptr)
		{
			rm->getUser()->joinRoom(getRoomById(atoi(roomId.c_str())));
			returnVal = true;
		}
		else
		{
			// the room was not found.
			rm->getUser()->send("1102");
			returnVal = false;
		}
	}
	return returnVal;
}


void TriviaServer::handleGetUsersInRoom(RecievedMessage * rm)
{
	string roomId = rm->getValues()[0];
	Room* room = getRoomById(stoi(roomId));

	if (room == nullptr)
		rm->getUser()->send("1080");
	else
		rm->getUser()->send(room->getUsersListMessage());
}


void TriviaServer::handleGetRooms(RecievedMessage * rm)
{
	string codeToClient = "106";
	string messageToClient;

	messageToClient = codeToClient + Helper::getPaddedNumber(_rooMIdSequence, 4);
	
	std::map<int, Room*>::iterator it;
	for (it = _roomsList.begin(); it != _roomsList.end(); ++it)
	{
		messageToClient += Helper::getPaddedNumber(it->first, 4) + Helper::getPaddedNumber(it->second->getName().length(), 2) + it->second->getName();
	}

	rm->getUser()->send(messageToClient);
}


bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	bool returnVal = false;

	User* user = msg->getUser();
	if(user == nullptr) // the msg dont have a user
	{
		returnVal = false;
	}
	else
	{
		Room* room = user->getRoom();
		if (room == nullptr) // the user dont have a room
		{
			returnVal = false;
		}
		else
		{
			msg->getUser()->leaveRoom();
			returnVal = true;
		}
	}
	return returnVal;
}


bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	bool returnVal = false;

	if (msg->getUser()->getRoom() == nullptr) // the user dont have a room
	{
		returnVal = false;
	}
	else
	{
		int roomId = msg->getUser()->closeRoom();
		if (roomId != -1) // the user have a room
		{
			_roomsList.erase(roomId);
			_rooMIdSequence--;
		}
		else
			returnVal = false;
	}
	return returnVal;
}