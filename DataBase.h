#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <sstream>
#include "Question.h"
#include "sqlite3.h"

class Question;

class DataBase
{
public:
	DataBase();
	~DataBase();

	bool isUserExists(std::string username); 
	bool addNewUser(std::string username, std::string password, std::string email); 
	bool isUserAndPassMatch(std::string username, std::string password);
	std::vector<Question*> initQuestions(int questionsNo);
	int insertNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime);

	// geters
	std::vector<std::string> getBestScores(); // to do
	std::vector<std::string> getPersonalStatus(std::string username); // to do

	// call back functions
	//static int callbackCount(void*, int, char**, char**);
	static int callbackQuestions(void*, int, char**, char**);
	/*static int callbackScores(void*, int, char**, char**);
	static int callbackStatus(void*, int, char**, char**);*/



private:
	sqlite3* _db; // the data base

};

